<span id="panel_0">
   <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
      <h1 class="display-4">Manage your Influencers</h1>
      <p class="lead">Quickly put a button on your hotel's website to manage the inflow of Influencers. Use A.I. to determine the best influencers and make special deals to viral your marketing and skyrocket returns!</p>
   </div>
   <div class="container">
   <div class="card-deck mb-3 text-center">
      <div class="card mb-4 box-shadow">
         <div class="card-header">
            <h4 class="my-0 font-weight-normal">Free</h4>
         </div>
         <div class="card-body">
            <h1 class="card-title pricing-card-title">$0 <small class="text-muted">/ mo</small></h1>
            <ul class="list-unstyled mt-3 mb-4">
               <li>Unlimited Buttons</li>
               <li>Emailed Leads</li>
               <li>Email Support</li>
            </ul>
            <button type="button" class="btn btn-lg btn-block btn-outline-primary" onclick="next()">Sign up for free</button>
         </div>
      </div>
      <div class="card mb-4 box-shadow">
         <div class="card-header">
            <h4 class="my-0 font-weight-normal">Pro</h4>
         </div>
         <div class="card-body">
            <h1 class="card-title pricing-card-title">$99 <small class="text-muted">/ mo</small></h1>
            <ul class="list-unstyled mt-3 mb-4">
               <li>A.I. Powered Leads</li>
               <li>Basic Analytics</li>
               <li>Basic Lead Management</li>
            </ul>
            <button type="button" class="btn btn-lg btn-block btn-primary" onclick="next()">Get started</button>
         </div>
      </div>
      <div class="card mb-4 box-shadow">
         <div class="card-header">
            <h4 class="my-0 font-weight-normal">Enterprise</h4>
         </div>
         <div class="card-body">
            <h1 class="card-title pricing-card-title">$499 <small class="text-muted">/ mo</small></h1>
            <ul class="list-unstyled mt-3 mb-4">
               <li>Full Service Lead Management</li>
               <li>Advanced Analytics</li>
               <li>Phone and Email support</li>
            </ul>
            <button type="button" class="btn btn-lg btn-block btn-primary" onclick="next()">Contact us</button>
         </div>
      </div>
   </div>
</div>
</span>

<span id="panel_1" style="display:none;">
   <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
      <h1 class="display-4">Enter your hotel's website</h1>
      <input class="mt-3 form-control form-control-lg" type="text" placeholder="www.theleelapalace.com">
      <button type="button" class="mt-3 btn btn-lg btn-block btn-primary" onclick="next()">Next</button>
   </div>
</span>
<span id="panel_2" style="display:none;">
   <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
      <h1 class="display-4">Enter your work email</h1>
      <input class="mt-3 form-control form-control-lg" type="text" placeholder="john@theleelapalace.com">
      <button type="button" class="mt-3 btn btn-lg btn-block btn-primary" onclick="next()">Next</button>
   </div>
</span>
<span id="panel_3" style="display:none;">
   <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
      <h1 class="display-4">Check your email</h1>
   </div>
</span>

<script>
Holder.addTheme('thumb', {
   bg: '#55595c',
   fg: '#eceeef',
   text: 'Thumbnail'
});
var start = 0;
function next() {

   $('#panel_'+start).hide();
   $('#panel_'+(start+1)).show()

   start = start + 1;

}
</script>
