   <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
      <h1 class="display-4">The Leela Palace</h1>
      <p class="lead">Influencer Application</p>
   </div>


<span id="panel_0" style="display:;">
   <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
      <h1 class="display-4">Enter your name</h1>
      <input class="mt-3 form-control form-control-lg" type="text" placeholder="John Cho">
      <button type="button" class="mt-3 btn btn-lg btn-block btn-primary" onclick="next()">Next</button>
   </div>
</span>
<span id="panel_1" style="display:none;">
   <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
      <h1 class="display-4">Enter your instagram username</h1>
      <input class="mt-3 form-control form-control-lg" type="text" placeholder="@john_the_fisherman">
      <button type="button" class="mt-3 btn btn-lg btn-block btn-primary" onclick="next()">Next</button>
   </div>
</span>
<span id="panel_2" style="display:none;">
   <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
      <h1 class="display-4">Save your account</h1>
   </div>
</span>
<script>
var start = 0;
function next() {

   $('#panel_'+start).hide();
   $('#panel_'+(start+1)).show()

   start = start + 1;

}
</script>