<?php include 'inc/header.php' ?>

<?php include 'inc/nav.php' ?>

	<?php 

		parse_str($_SERVER['QUERY_STRING'], $output);

		if (isset($output["view"])) {

			if ($output["view"] === 'map'){

				include 'views/map.php';

			} else {

				include 'views/index.php';

			}

		} else {
	
			include 'views/index.php';

		}

	?>

<?php include 'inc/footer.php' ?>







