<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Buttons by Trepic">
    <meta name="author" content="Derek Leavitt">
    <link rel="icon" href="img/favicon.ico">

    <title>Buttons by Trepic</title>

    <link href="vendor/bootstrap.min.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">
    
  </head>

  <body>