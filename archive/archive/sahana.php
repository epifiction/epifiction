<html>

<head>

  <title>Sahana</title>

  <style>

    body {
      margin: 0 0 0 0;
      padding: 0 0 0 0;
    }

    #output {
      width:100%;
      height: 100vh;
    }

    .box {
      height: 50px;
      width: 50px;
      float: left;
      background-image: url("3.jpg");
    }

  </style>

</head>

<body>

  <div id="output"></div>

</body>
</html>

<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/randomcolor/0.5.4/randomColor.js"></script>

<script>

  var runs = 200;

  for (var i = 0; i < runs; i ++) {

    $('#output').append('<div class="box"></div>')

  }

  setInterval(function(){

    console.log('1')

    $('.box').each(function(element){

      var a = Math.floor(Math.random()*100);
      var b = Math.floor(Math.random()*100);

      var string = a+"% "+b+"%";

      console.log(string)

      $(this).css("background-position", string);

    })

  }, 1000);

</script>