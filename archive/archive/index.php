<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Exclusive Content by Trepic</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  </head>
  <body>
    
<script>
var token;

// This is called with the results from from FB.getLoginStatus().
function statusChangeCallback(response) {
    console.log('statusChangeCallback');
    // console.log(response);
    token = response.authResponse.accessToken;
    // console.log("token: "+token);
    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    if (response.status === 'connected') {
        // Logged into your app and Facebook.
        testAPI();
    } else {
        // The person is not logged into your app or we are unable to tell.
        document.getElementById('status').innerHTML = 'Please log ' +
            'into this app.';
    }
}

// This function is called when someone finishes with the Login
// Button.  See the onlogin handler attached to it in the sample
// code below.
function checkLoginState() {
    FB.getLoginStatus(function(response) {
        statusChangeCallback(response);
    });
}

window.fbAsyncInit = function() {
    FB.init({
        appId: '629508394136443',
        cookie: true, // enable cookies to allow the server to access 
        // the session
        xfbml: true, // parse social plugins on this page
        version: 'v3.3' // The Graph API version to use for the call
    });

    // Now that we've initialized the JavaScript SDK, we call 
    // FB.getLoginStatus().  This function gets the state of the
    // person visiting this page and can return one of three states to
    // the callback you provide.  They can be:
    //
    // 1. Logged into your app ('connected')
    // 2. Logged into Facebook, but not your app ('not_authorized')
    // 3. Not logged into Facebook and can't tell if they are logged into
    //    your app or not.
    //
    // These three cases are handled in the callback function.

    FB.getLoginStatus(function(response) {
        statusChangeCallback(response);
    });

};

// Load the SDK asynchronously
(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s);
    js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

// Here we run a very simple test of the Graph API after login is
// successful.  See statusChangeCallback() for when this call is made.
function testAPI() {
    console.log('Welcome!  Fetching your information.... ');
    FB.api('/me', function(response) {
        console.log('Successful login for: ' + response.name);
        document.getElementById('status').innerHTML =
            'Thanks for logging in, ' + response.name + '!';
    });
}


var long;

function long() {

    $.post('http://localhost/api.php', {
        sugar: token
    }, function(response) {

        var output = JSON.parse(response);

        long = output.access_token;

        $('#output').append("FB Access Token:<br>" + long + "<br>");

    });

};

function pages() {
    $.get('https://graph.facebook.com/v3.2/me/accounts?access_token=' + long, function(response) {

        for (var i = 0; i < response.data.length; i++) {

            instagram(response.data[i].access_token, response.data[i].id);

        }

    });

}

var instagrams = [];

var code;


function instagram(apple, orange) {


    $.get('https://graph.facebook.com/v3.2/' + orange + '?fields=instagram_business_account&access_token=' + apple, function(response) {

        if (response.instagram_business_account) {

            instagrams.push({"id":response.instagram_business_account.id,"token":apple});

        }

    });

}

function insights(she, buy) {


    for (var i = 0; i < instagrams.length; i++) {

$.get('https://graph.facebook.com/v3.2/'+instagrams[i].id+'/insights?metric=impressions,reach,profile_views&period=day&access_token='+instagrams[i].token, function(response) {

  console.log(response);

});



    }

}


</script>

<fb:login-button scope="public_profile,email,instagram_basic,instagram_manage_insights,manage_pages" onlogin="checkLoginState();">
</fb:login-button>

<div id="status">
</div>

<button onclick="long()">Get Long Term Access Token</button>
<button onclick="pages()">Get Instagram Accounts</button>
<button onclick="insights()">Get Insights</button>

<div id="output"></div>

  </body>
</html>
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
