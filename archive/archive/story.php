<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Trepic | Story Creator</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  </head>
  <body>
    
    <div class="container" id="story" style=" min-height: 250px;">

        <div style="text-align:center;"><h1>Trepic Story/Itinerary Creator</h1></div>

        <div class="form-group">
          <label for="exampleInputEmail1">What is the name of your story?</label>
          <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Cave diving in Tulum">
        </div>

        <div class="form-group">
          <label for="exampleInputEmail1">Who went on this story with you?</label>
          <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="John Cho">
        </div>

        <div class="form-group">
          <label for="exampleInputEmail1">What inspired you to take this trip?</label>
          <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="I got the idea from Trepic">
        </div>

        <hr>

    </div>

    <div class="container" id="options" style="display:none;">
      
      <button type="button" class="btn btn-dark" onclick="option('photo')">Photo</button>
      <button type="button" class="btn btn-primary" onclick="option('video')">Video</button>
      <button type="button" class="btn btn-secondary" onclick="option('paragraph')">Paragraph</button>
      <button type="button" class="btn btn-success" onclick="option('hotel')">Hotel</button>
      <button type="button" class="btn btn-danger" onclick="option('restarant')">Restaraunt</button>
      <button type="button" class="btn btn-warning" onclick="option('destination')">Destination</button>

      <hr>

    </div>

    <div class="container" id="controls" style="margin-bottom:250px;">

      <button type="button" class="btn btn-primary btn-lg btn-block" onclick="add()">Add</button>
<!--       <button type="button" class="btn btn-secondary btn-lg btn-block" onclick="save()">Save</button> -->

    </div>

  </body>
</html>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

<script>

  function add(){

    $('#options').show();
    $('#controls').hide();

  };

  // function save(){

  //   alert('saved')

  // };

  function option(type){

    $('#options').hide();

    if (type === 'photo'){

      $('#story').append('<img src="https://placeimg.com/640/480/nature" style="width:50%;">');
      $('#story').append('<div class="form-group"><label for="exampleInputEmail1">Caption this Photo</label><input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nature is so beautiful"></div>');
      $('#story').append('<hr>')


    } else if (type === 'video') {

      $('#story').append('<img src="https://beamimagination.com/wp-content/uploads/2017/09/video-placeholder.png" style="width:33%;">');
      $('#story').append('<div class="form-group"><label for="exampleInputEmail1">Caption this Video</label><input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Me scubadiving underwater"></div>');
      $('#story').append('<hr>')



    } else if (type === 'paragraph') {

      $('#story').append('<div class="form-group"><label for="exampleFormControlTextarea1">Write your story here</label><textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea></div>')
      $('#story').append('<hr>');

    } else if (type === 'hotel') {

      $('#story').append('<div class="form-group"><label for="exampleInputEmail1">What is the name of your Hotel?</label><input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Cave diving in Tulum"></div>')
      $('#story').append('<hr>');


    } else if (type === 'restarant') {

      $('#story').append('<div class="form-group"><label for="exampleInputEmail1">What is the name of your restarant?</label><input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Cave diving in Tulum"></div>')
      $('#story').append('<hr>');

    } else if (type === 'destination') {

      $('#story').append('<div class="form-group"><label for="exampleInputEmail1">What is the name of your destination?</label><input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Cave diving in Tulum"></div>')
      $('#story').append('<hr>');

    }

    $('#controls').show();


  };

</script>