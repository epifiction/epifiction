<!doctype html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <title>Buttons by Trepic</title>
      <link rel="shortcut icon" href="img/logo.png">
      <link rel="stylesheet" href="vendor/bootstrap.css">
   </head>
   <body class="invert">

      <div class="container">

      <H1>Embed our button on your website!</H1>

        <a href="#menu" class="menu-link" style="">
          <img src="img/logo.png" style="height:50px;">
        </a>

      </div>

   </body>
</html>

<script src="vendor/jquery.js"></script>
<script src="vendor/popper.js"></script>
<script src="vendor/bootstrap.js"></script>
<script src="vendor/slide.js"></script>
<script>

    $(document).ready(function() {
        $('.menu-link').bigSlide();
    });

</script>