<!doctype html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <title>GoAnywhere by Trepic</title>
      <link rel="shortcut icon" href="img/logo.png">
      <link rel="stylesheet" href="vendor/bootstrap.css">
   </head>
   <body class="invert">

      <div class="container">

      <H1>Embed the Trepic GoAnywhere Button on your blog of choice!</H1>

                <a href="#menu" class="menu-link" style="">
            <img src="img/logo.png" style="height:50px;">
          </a>
      <br>

      <pre><code>&lt;embed&gt;embed();function embed(){$('trepic-main')}&lt;/embed&gt;
</code></pre>

</div>

       <nav id="menu" class="panel" role="navigation" style="z-index:300;">
            <a href="#"><img class="pong" src="img/ads/1.png" style="height:75px;"></a></li>
            <a href="#"><img class="pong" src="img/ads/2.png" style="height:75px;"></a></li>
            <a href="#"><img class="pong" src="img/ads/3.png" style="height:75px;"></a></li>
            <a href="#"><img class="pong" src="img/ads/4.png" style="height:75px;"></a></li>
                        <a href="#"><img class="pong" src="img/ads/1.png" style="height:75px;"></a></li>
            <a href="#"><img class="pong" src="img/ads/2.png" style="height:75px;"></a></li>
            <a href="#"><img class="pong" src="img/ads/3.png" style="height:75px;"></a></li>
            <a href="#"><img class="pong" src="img/ads/4.png" style="height:75px;"></a></li>
                        <a href="#"><img class="pong" src="img/ads/1.png" style="height:75px;"></a></li>
            <a href="#"><img class="pong" src="img/ads/2.png" style="height:75px;"></a></li>
            <a href="#"><img class="pong" src="img/ads/3.png" style="height:75px;"></a></li>
            <a href="#"><img class="pong" src="img/ads/4.png" style="height:75px;"></a></li>
                        <a href="#"><img class="pong" src="img/ads/1.png" style="height:75px;"></a></li>
            <a href="#"><img class="pong" src="img/ads/2.png" style="height:75px;"></a></li>
            <a href="#"><img class="pong" src="img/ads/3.png" style="height:75px;"></a></li>
            <a href="#"><img class="pong" src="img/ads/4.png" style="height:75px;"></a></li>

            </ul>
         </nav>

   </body>
</html>

<script src="vendor/jquery.js"></script>
<script src="vendor/popper.js"></script>
<script src="vendor/bootstrap.js"></script>
<script src="vendor/slide.js"></script>
<script>

    $(document).ready(function() {
        $('.menu-link').bigSlide();
    });

</script>