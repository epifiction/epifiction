<!doctype html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <title>Business Cards by Trepic</title>
      <link rel="shortcut icon" href="img/logo.png">
      <link rel="stylesheet" href="vendor/bootstrap.css">
      <link rel="stylesheet" href="vendor/cards/css/style.css">
   </head>
   <body>
      <div class="stage">
         <!-- <div class="title">Business Cards by Trepic</div> -->
         <div id="stacked-cards-block" class="stackedcards stackedcards--animatable init">
            <div class="stackedcards-container">
               <div class="card">
                  <div class="card-content">
                     <div class="card-image"><img src="https://image.ibb.co/fXPg7n/Beach_and_Chill.png" width="100%" height="100%"/></div>
                     <div class="card-titles">
                        <h1>Neethusha<br/>@ella_gh</h1>
                        <h3>12.6k Followers</h3>
                     </div>
                  </div>
                  <div class="card-footer">
                     <div class="popular-destinations-text">360 Videos</div>
                     <div class="popular-destinations-images">
                        <div class="circle"><img src="https://image.ibb.co/muiA07/beach_chill_1.jpg" width="100%" height="100%"/></div>
                        <div class="circle"><img src="https://image.ibb.co/emAOL7/beach_chill_2.jpg" width="100%" height="100%"/></div>
                        <div class="circle"><img src="https://image.ibb.co/invq07/beach_chill_3.jpg" width="100%" height="100%"/></div>
                     </div>
                  </div>
                  <div class="card-footer">
                     <div class="popular-destinations-text">Analyze</div>
                     <div class="popular-destinations-images">
                        <a href="https://hypeauditor.com/preview/neethusha.music/" target="_blank"><img src="img/hype.png" width="100%" height="100%"/></a>
                     </div>
                  </div>
               </div>
               <div class="card">
                  <div class="card-content">
                     <div class="card-image"><img src="img/vishnu.jpg" width="100%" height="100%"/></div>
                     <div class="card-titles">
                        <h1>The Leela Palace<br/>@Vishnu</h1>
                        <!-- <h3>1,000 Rooms</h3> -->
                        <img src="img/stars.png" style="height:30px;">
                     </div>
                  </div>
                  <div class="card-footer">
                     <div class="popular-destinations-text">360 Videos</div>
                     <div class="popular-destinations-images">
                        <div class="circle"><img src="https://image.ibb.co/muiA07/beach_chill_1.jpg" width="100%" height="100%"/></div>
                        <div class="circle"><img src="https://image.ibb.co/emAOL7/beach_chill_2.jpg" width="100%" height="100%"/></div>
                        <div class="circle"><img src="https://image.ibb.co/invq07/beach_chill_3.jpg" width="100%" height="100%"/></div>
                     </div>
                  </div>
                  <div class="card-footer">
                     <div class="popular-destinations-text">Analyze</div>
                     <div class="popular-destinations-images">
                        <a href="https://www.tripadvisor.com/" target="_blank"><img src="img/ads/1.png" width="100%" height="100%"/></a>
                        <a href="https://www.trivago.com/" target="_blank"><img src="img/ads/2.png" width="100%" height="100%"/></a>
                        <a href="https://www.booking.com/" target="_blank"><img src="img/ads/3.png" width="100%" height="100%"/></a>
                        <a href="https://www.expedia.com/" target="_blank"><img src="img/ads/4.png" width="100%" height="100%"/></a>
                     </div>
                  </div>
               </div>
               <div class="card">
                  <div class="card-content">
                     <div class="card-image"><img src="https://image.ibb.co/jY88nn/city_breaks.png" width="100%" height="100%"/></div>
                     <div class="card-titles">
                        <h1>City <br/> Breaks</h1>
                        <h3>32 Destinations</h3>
                     </div>
                  </div>
                  <div class="card-footer">
                     <div class="popular-destinations-text">360 Videos</div>
                     <div class="popular-destinations-images">
                        <div class="circle"><img src="https://image.ibb.co/muiA07/beach_chill_1.jpg" width="100%" height="100%"/></div>
                        <div class="circle"><img src="https://image.ibb.co/emAOL7/beach_chill_2.jpg" width="100%" height="100%"/></div>
                        <div class="circle"><img src="https://image.ibb.co/invq07/beach_chill_3.jpg" width="100%" height="100%"/></div>
                     </div>
                  </div>
                  <div class="card-footer">
                     <div class="popular-destinations-text">Analyze</div>
                     <div class="popular-destinations-images">
                        <a href="https://www.tripadvisor.com/" target="_blank"><img src="img/ads/1.png" width="100%" height="100%"/></a>
                        <a href="https://www.trivago.com/" target="_blank"><img src="img/ads/2.png" width="100%" height="100%"/></a>
                        <a href="https://www.booking.com/" target="_blank"><img src="img/ads/3.png" width="100%" height="100%"/></a>
                        <a href="https://www.expedia.com/" target="_blank"><img src="img/ads/4.png" width="100%" height="100%"/></a>
                     </div>
                  </div>
               </div>
               <div class="card">
                  <div class="card-content">
                     <div class="card-image"><img src="https://image.ibb.co/eBNZSn/Family_Vacation.png" width="100%" height="100%"/></div>
                     <div class="card-titles">
                        <h1>Family <br/> Vancation</h1>
                        <h3>20 Destinations</h3>
                     </div>
                  </div>
                  <div class="card-footer">
                     <div class="popular-destinations-text">360 Videos</div>
                     <div class="popular-destinations-images">
                        <div class="circle"><img src="https://image.ibb.co/muiA07/beach_chill_1.jpg" width="100%" height="100%"/></div>
                        <div class="circle"><img src="https://image.ibb.co/emAOL7/beach_chill_2.jpg" width="100%" height="100%"/></div>
                        <div class="circle"><img src="https://image.ibb.co/invq07/beach_chill_3.jpg" width="100%" height="100%"/></div>
                     </div>
                  </div>
                  <div class="card-footer">
                     <div class="popular-destinations-text">Analyze</div>
                     <div class="popular-destinations-images">
                        <a href="https://www.tripadvisor.com/" target="_blank"><img src="img/ads/1.png" width="100%" height="100%"/></a>
                        <a href="https://www.trivago.com/" target="_blank"><img src="img/ads/2.png" width="100%" height="100%"/></a>
                        <a href="https://www.booking.com/" target="_blank"><img src="img/ads/3.png" width="100%" height="100%"/></a>
                        <a href="https://www.expedia.com/" target="_blank"><img src="img/ads/4.png" width="100%" height="100%"/></a>
                     </div>
                  </div>
               </div>
            </div>
            <div class="stackedcards--animatable stackedcards-overlay top"><img src="https://image.ibb.co/m1ykYS/rank_army_star_2_3x.png"  width="auto" height="auto"/></div>
            <div class="stackedcards--animatable stackedcards-overlay right"><img src="https://image.ibb.co/dCuESn/Path_3x.png" width="auto" height="auto"/></div>
            <div class="stackedcards--animatable stackedcards-overlay left"><img src="https://image.ibb.co/heTxf7/20_status_close_3x.png" width="auto" height="auto"/></div>
         </div>
         
         <div class="global-actions">
            <div class="left-action"><img src="https://image.ibb.co/heTxf7/20_status_close_3x.png" width="26" height="26"/></div>
            <div class="top-action"><img src="https://image.ibb.co/m1ykYS/rank_army_star_2_3x.png" width="18" height="16"/></div>
            <div class="right-action"><img src="img/hand.png" width="30" height="28"/></div>
         </div>

      </div>

      <div class="final-state hidden">
         <div class="title">
            <h1>Match!</h1>
            <br>
            <div class="container" style="max-width: 61.8%;">
               <form>
                  <div class="form-group">
                     <label for="exampleInputEmail1">Email Address</label>
                     <input type="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Enter email">
                     <small id="emailHelp" class="form-text text-muted">We don't verify email accounts but you'll need a correct one to contact matches- which, we will do by introducing you by email in person.</small>
                  </div>
                  <div class="form-group">
                     <label for="exampleInputPassword1">Password (Optional)</label>
                     <input type="password" class="form-control" id="password" placeholder="Password">
                     <small id="emailHelp" class="form-text text-muted">This is optional if you want to stay logged in and keep track of your matches.</small>
                  </div>
                  <div class="form-check">
                     <input type="checkbox" class="form-check-input" id="exampleCheck1">
                     <label class="form-check-label" for="exampleCheck1">I Agree</label>
                  </div>
                  <button type="submit" class="btn btn-primary" onclick="gmail();">Submit</button>
               </form>
            </div>
         </div>
      </div>

   </body>
</html>
<script src="vendor/jquery.js"></script>
<script src="vendor/popper.js"></script>
<script src="vendor/bootstrap.js"></script>
<script src="vendor/cards/js/index.js"></script>

<script>

var cardz = [];

cardz.push({"a":"https://image.ibb.co/gQsq07/Adventure_and_Outdoor.png",
"b":"Adventure ",
"c":" and Outdoor",
"d":"10 Destinations",
"e":"https://image.ibb.co/jmEYL7/adventure_1.jpg",
"f":"https://image.ibb.co/nsCynn/adventure_2.jpg",
"g":"https://image.ibb.co/hmsL07/adventure_3.jpg"})

console.log(cardz[0].a)
var runs = 0; 

for (var i = 0; i < runs; i++){

  $('.stackedcards-container').append('<div class="card"><div class="card-content"><div class="card-image"><img src="'+cardz[0].a+'" width="100%" height="100%"/></div><div class="card-titles"><h1>'+cardz[0].b+'<br/>'+cardz[0].c+'</h1><h3>'+cardz[0].d+'</h3></div>  </div><div class="card-footer"><div class="popular-destinations-text">Popular <br/> Destinations</div><div class="popular-destinations-images"><div class="circle"><img src="'+cardz[0].e+'" width="100%" height="100%"/></div><div class="circle"><img src="'+cardz[0].f+'" width="100%" height="100%"/></div><div class="circle"><img src="'+cardz[0].g+'" width="100%" height="100%"/></div></div></div></div>');

}

function gmail(){



var email = $('#email').val();
var password = $('#password').val();

  $.post('http://localhost/archive/api/mail.php', {

        email: email,
        password: password

            }, function(response) {

              alert('Check your Email!');
              console.log(response)
              location.reload();

    });

}


</script>
<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
  (adsbygoogle = window.adsbygoogle || []).push({
    google_ad_client: "ca-pub-2337615406873247",
    enable_page_level_ads: true
  });
</script>
